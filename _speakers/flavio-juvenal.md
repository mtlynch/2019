---
name: Flávio Juvenal
talks:
- "Django + ElasticSearch without invalidation logic"
---
Flávio is a software engineer from Brazil and partner at Vinta Software
(www.vinta.com.br). At Vinta, Flávio builds high-quality web products, from
UX to code, using mostly React and Django. Flávio loves coffee and is always
looking for good coffee beans with exotic flavor profiles.
