---
name: Vanessa Barreiros
talks:
- "Building effective Django queries with expressions"
---
I'm a full stack developer at [Vinta Software](https://www.vinta.com.br),
and Django Girls organizer at my hometown, Recife. When not coding and
working on personal projects, I'm usually cooking and watching Grey's
Anatomy.
