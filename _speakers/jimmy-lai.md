---
name: Jimmy Lai
talks:
- "Code Quality at Scale"
---
Jimmy Lai is a Software Engineer in Instagram Infrastructure. He developed
in Python and shared his experiences in PyCon TW and APAC since 2012. His
recent interests include Python efficiency profiling, optimization and
asyncio. He also contributed a few asyncio bug fix and optimation to
CPython. This year, he plans to share his efficiency experiences learned
from large scale Python web application - Instagram.
