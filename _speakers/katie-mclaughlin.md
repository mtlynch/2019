---
name: Katie McLaughlin
talks:
- "What is deployment, anyway? "
---
Katie has worn many different hats over the years. She has been a software
developer for many programming languages, systems administrator for multiple
operating systems, and speaker on many different topics.

She is currently a Cloud Developer Advocate at Google, a Director of the
Python Software Foundation, a Director of the Django Software Foundation,
and Conference Director for PyCon AU 2018/2019. She won the O’Reilly Open
Source Award in 2017, and was a finalist in the Red Hat Women in Open Source
Award in 2018.

When she’s not changing the world, she enjoys cooking, making tapestries,
and seeing just how well various application stacks handle emoji.
