---
name: Nicole Zuckerman
talks:
- "The promised Django Land; the tale of one team\u2019s epic journey from the country of Flask to Django Land"
---

Nicole attended Hackbright Academy in 2012, and has been in love with python
ever since. They can currently be found at Clover Health, tackling with glee 
data pipelines and APIs and everything in between. In their free time, Nicole is
an avid dancer and teacher, sci-fi book fanatic, soul and jazz aficionado, 
and cheese lover. They have an MA in English Literature and Women’s Studies from
the University of Liverpool.
