---
duration: 25
presentation_url:
room:
slot:
speakers:
- Andrew Knight
title: "Beyond Unit Tests: End-to-End Web UI Testing"
type: talk
video_url:
---
Unit tests are great, but they don’t catch all bugs because they don’t test
features like a user. However, Web UI tests are complicated and notoriously
unreliable. Plus, the variety of browsers and versions can feel
overwhelming. So, how can we write tests well? Never fear!

Let’s learn how to write robust, scalable Web UI tests using Python, pytest,
and Selenium WebDriver that cover the full stack for any Web app. In this
talk, we’ll cover:

* Using Selenium WebDriver like a pro
* Modeling Web UI interactions in Python code
* Handling multiple browsers
* Writing “good” feature tests that are efficient, robust, and readable
* Deciding what should and should not be tested with automation


After this talk, you’ll be able to write battle-hardened Web UI tests for
any Web app, including Django and Flask apps. There will be plenty of
example code available, too.
