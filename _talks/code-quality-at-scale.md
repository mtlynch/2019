---
duration: 40
presentation_url:
room:
slot:
speakers:
- Jimmy Lai
title: "Code Quality at Scale"
type: talk
video_url:
---
Our codebase, one of the largest Python application in the world, grew to
more than 3 million SLOC in 7 years.  Maintaining the code quality became
challenging when more developers joined the team. Tons of abandoned code
spreading around not only reduced readability but also wasted CPU resource
at runtime. We found engineers usually optimized their work for product
feature deadline and focused less on Code Quality. Code quality work like
code cleanup and refactoring were not fun and didn't seem impactful. To
address the problem fundamentally, we applied two strategies:

* Increase Visibility:
    * Problem: Without tooling support, it's not straightforward to
      understand the impact of poor code quality. It's also not clear
      where the opportunities are and how to contribute.
    * Solution: Build profilers through static analysis and runtime
      analysis to collect metrics and opportunities. Surface those
      opportunities into development process with tooling including linter
      in editor and dashboard.
* Recognize Contribution:
    * Problem: Individual contribution on code quality are usually not
      measured. Engineers have less incentive to contribute if their
      contribution won't be recognized.
    * Solution: Build commit analysis pipeline to collect contribution
      metrics automatically. Recognize contributors and lead by examples.
      Host fixathon events to improve code quality together and have fun.


Instead of blaming on debt creators, we encouraged everyone to contribute
and make impact. Code cleanup was the major focus and it became a fun game.
In 6 months, 2000 pull requests were shipped to clean up code which removed
150,000+ SLOC and saved 5%+ CPU resource at runtime. After this talk, you'll
have learned:

* Common code quality issues in large scale codebase.
* Build tooling to collect metrics, identify code quality opportunities and
  track contribution.
* Change company culture and build a sustainable codebase by recognizing
  code quality contribution.
