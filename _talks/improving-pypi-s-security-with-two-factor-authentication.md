---
duration: 40
presentation_url:
room:
slot:
speakers:
- William Woodruff
title: "Improving PyPI\u0027s security with Two Factor Authentication"
type: talk
video_url:
---
Since March, [Trail of Bits](https://www.trailofbits.com) has worked with
the PSF to implement and land major security improvements in
[Warehouse](https://github.com/pypa/warehouse/), the codebase that drives
[PyPI](https://pypi.org). This talk will cover just one of those
improvements: the addition of two factor authentication to user logins.
Attendees will learn about the technical details of two factor schemes, the
security properties they can (and can not) provide, and the process for
making major changes to core Python infrastructure.
